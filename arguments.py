def avg(*arg):
    sum = 0
    for i in arg:
        sum += i
    return sum / len(arg)


def pretty_print(**kwargs):
    for key, value in kwargs.items():
        print(f"{key} ---> {value}")

