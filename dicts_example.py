# O(n)

d1 = {
    "key1": 1,
    "key2": 2,
}

d2 = dict((("key1", 1), ("key2", 2)))

d3 = dict(key1=1, key2=2)

d3["key1"]

d4 = {}

d4["name"] = "Olga"
d4["age"] = 30
d4["position"] = "SMM manager"


# ключі словника

# obj.__hash__()

class ListHash(list):
    def __hash__(self):
        return hash(12)

# iterations

d5 = {
    "one": 11,
    "two": 22,
    "three": 33
}

for key, value in d5.items():
    print(f"key: {key} --- value: {value}")
